import { RequestHandler } from "express";
import { model } from 'mongoose';
import { UserSchema } from '../models/userModel';
import { User } from '../models/userModel';

const UserModel = model('User', UserSchema);

export const addNewUser = async (user: Omit<User, "created_date">) => {
    
    let newUser = new UserModel(user);
    // TODO: Valida business Rules como Pass valida (mas de 8 caracteres), etc nombre sea mayor a 3 y solo caracteres sin nros
    return await newUser.save();        
}

export const getUsers = async ()  => {
     return await UserModel.find({});
}

export const getUserWithID: RequestHandler = (req, res) => {
    UserModel.findById(req.params.userID, null, null, (err, user) => {
        if (err) {
            res.send(err);
        }
        res.json(user);
    });
}

export const updateUser: RequestHandler = (req, res) => {
    UserModel.findOneAndUpdate(
        { _id: req.params.userID},
        req.body,
        { new: true, useFindAndModify: false },
        (err, user) => {
            if (err) {
                res.send(err);
            }
            res.json(user);
        }
    );
}

export const deleteUser: RequestHandler = (req, res) => {
    UserModel.remove({ _id: req.params.userID}, err => {
        if (err) {
            res.send(err);
        }
        res.json({ message: 'successfuly deleted user'});
    });
}
